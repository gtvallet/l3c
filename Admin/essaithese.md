---
visibility: protected
label: Informations sur les essais et les thèses
order: 1000
author: GT Vallet
---

## Informations institutionnelles 
### Essai doctoral (D.Psy.)
https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=5197&owa_no_fiche=6

### Thèse de doctorat (R/I et Ph.D. recherche)
https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=5197&owa_no_fiche=9&owa_bottin=
