---
visibility: protected
label: Finances
order: 1000
author: GT Vallet & Alexandre Descoteaux
---


# Avance de fonds
Les avances de fonds permettent de recevoir une somme d'argent sur son compte personnel afin de couvrir des dépenses à venir.
Une avance de fonds est typiquement utile pour dédommager les personnes qui participent. 
Vous pouvez ainsi effectuer les virements interacts (ou autre moyen) sans devoir utiliser votre argent.

!!!info
Le  délai de traitement de la demande est d'environ 1 mois !
!!!

## Faire une demande d'avance 
Une demande d'avance de fonds doit être envoyée aux services des finances en vertu de l'article 11.3 de la *Politique sur le remboursement des frais de déplacement, de séjour, de représentation et autres dépenses* de l'UQTR.
Le montant minimal applicable est de *200\$ CAD*. 
Après autorisation, le montant demandé sera déposé dans votre compte bancaire et vous disposerez de **4 mois** à partir de la date du versement pour utiliser les fonds et produire un rapport de dépense. 
Si ce délai n’est pas respecté, le montant non utilisé devra être remboursé à l’UQTR. 
Au besoin, une nouvelle avance peut être demandée.

La première étape est de convenir avec la direction de recherche du montant à demander (nombre de personnes) selon le montant énoncé dans la demande d'éthique.

1. Ouvrir le [fichier Demande d’avance](https://uqtrsspt-my.sharepoint.com/:x:/g/personal/guillaume_vallet_uqtr_ca/EQ7OjRMyCMFHjfEksa3yCvABql1KC8F_tyszQisxD-YC6Q?e=MaVCda)
2. Indiquer votre nom comme réquérant
3. Indiquer votre numéro de matricule (si vous avez déjà eu un contrat avec l'UQTR, voir son dossier employé *SAFIRH* (Portail -> Membre du personnel -> Ressources humaines -> SAFIRH : Portail des employés), sinon indiquer l'adresse postale complète 
4. Indiquer le montant demandé et justifier la demande
5. Indiquer le numéro de certificat éthique 
6. Indiquer l’UBR concerné (voir Guillaume)
7. Ajouter votre signature
8. Envoyer à Guillaume pour vérification, l'ajout de l'UBR, et l'ajout de sa signature
9. Envoyer à (Julie Beauchemin)[mailto:julie.beauchemin@uqtr.ca] (avec Guillaume en copie)

!!!secondary
[Exemple de demande d'avance](https://uqtrsspt-my.sharepoint.com/:b:/g/personal/guillaume_vallet_uqtr_ca/ES2nvGO5La5BreA8yPeDNYQB2ElOZZJAqBrGQ5jV_kYrbQ?e=oevisw)
!!!

## Passations et suivi des fonds
Durant les passations, il est essentiel d'assurer une bonne gestion des fonds et de consigner certaines informations nécessaires pour le suivi des fonds. 
Le dédommagement des personnes participantes peut se faire grâce à un virement Interact ou par paiement par argent comptant.
Le laboratoire recommande fortement d'utiliser les virements Interact pour assurer un suivi des échanges des fonds.

### Virement Interac (recommandé)
1. **Pièces justificatives**  
Consignez la preuve de chaque virement effectué au fur et à mesure, car ces documents devront être joints au rapport de dépenses à la fin du recrutement. Les noms doivent être masqués, puisqu'il s'agit d'informations confidentielles. 
Vous pouvez biffer cette information ou enregistrer la personne sous son code participant dans votre banque. 

!!!info
Utilisez des outils comme "Edit PDF" de [TinyWow](https://tinywow.com/) pour masquer les informations confidentielles de vos relevés bancaires.
!!!

2. **Consignation des informations**  
Consignez dans un tableur type Excel les codes participants, la/les dates de participation, la date d'envoi du paiement et le montant envoyé.

!!!WARNING
Il faut garder la trace des virements interacts (à fournir à l'UQTR) et mettre à jour le fichier de suivi à chaque rencontre.
!!!

### Argent comptant 
1. **Pièces justificatives**  
Un relevé bancaire prouvant que l'argent a été retiré de votre compte devra être joint à votre rapport de dépenses à la fin du projet. 
Lorsque le montant est conséquent, il est suggéré de le retirer en plusieurs fois. 
Par exemple, une avance de fonds de 900 \$ pourrait être retirée en 3 montants de 300 \$ afin de réduire le risque de vol ou de perte, tout en limitant le nombre de pièces justificatives à produire.

2.  **Consignation des informations**  
Consignez dans un tableur type Excel les codes participants, la/les dates de participation, la date d'envoi du paiement et le montant envoyé.

3.  **Reçus**  
Faites signer un reçu lors de la remise de la compensation afin de vous protéger advenant le cas où un participant reviendrait contre vous pour non-paiement.

!!!WARNING
Les reçus signés **ne doivent pas** être envoyés au [service des finances](http://www.uqtr.ca/servicedesfinances), puisqu'ils contiennent les noms des participants. Ils doivent être conservés sous clé et uniquement accessibles par les membres de l'équipe de recherche, conformément aux politiques de confidentialité.
!!!

!!!secondary
[Exemple de reçus](https://uqtrsspt-my.sharepoint.com/:w:/g/personal/guillaume_vallet_uqtr_ca/EV1iB8RhJ1VFjN-MG-YUHxcBO_oh_cUYVXgtahcUNqLMnA?e=arNhS0)
!!!

### Rapport de dépenses
Lorsque toutes les personnes participantes ont été rencontrées, ou que le délai maximal de 4 mois est écoulé, il faudra remplir le formulaire Excel [fichier demande de remboursement](https://uqtrsspt-my.sharepoint.com/:x:/g/personal/guillaume_vallet_uqtr_ca/EdsbfWntxZhLgdZc4eLxzZQBgytKJ29fw6HP27kWR4HxzA?e=vo00KM) 

### Procédure
1.  L'objet du rapport de dépenses doit inclure le titre du projet et le numéro de certificat d’éthique de recherche avec des êtres humains.
2.  Incrire la date de passation à la colonne "Date" suivi du code participant dans la colonne "Détails".
3.  Indiquer l'UBR se retrouvant sur la demande d'avance de fonds (voir avec direction de recherche au besoin) et signer le formulaire.
4.  Envoyer le formulaire et les pièces justificatves pertinentes au département des finances [rdepfinances\@uqtr.ca](mailto:rdepfinances@uqtr.ca){.email}.

!!!INFO
[Exemple de rapport de dépense](https://uqtrsspt-my.sharepoint.com/:b:/g/personal/guillaume_vallet_uqtr_ca/EfC5hMp_uUtMjkEkODHbvnIB03tWn7nLf92GwAx1lWLMMw?e=NCvV5n)
!!!

### Fonds résiduels
Si des fonds sont restants à la fin du projet, vous devez produire un rapport de dépenses (voir ci-dessus) et communiquer avec le département des finances, qui vous indiquera la marche à suivre pour procéder au remboursement du solde résiduel.



# Remboursement de frais (affiches, congrès...)
Plusieurs situations peuvent survenir dans laquelle une demande d'avance n'est pas souhaitable ou ne peut avoir être faite à temps.
Dans ces cas-là, il convient d'effectuer une demande de remboursement de frais.

1. Ouvrir le [fichier demande de remboursement](https://uqtrsspt-my.sharepoint.com/:x:/g/personal/guillaume_vallet_uqtr_ca/EdsbfWntxZhLgdZc4eLxzZQBgytKJ29fw6HP27kWR4HxzA?e=vo00KM) 
2. Indiquer votre nom comme réquérant
3. Indiquer votre numéro de matricule (si vous avez déjà eu un contrat avec l'UQTR, sinon indiquer l'adresse postale complète) 
4. Indiquer les éléments à rembourser et les montants associés
5. Indiquer l’UBR concerné (voir Guillaume)
6. Ajouter votre signature 
7. Préparer/numériser et joindre les justificatifs de dépenses (factures, tickets de paiement) et justificatif de présence pour un congrès (attestation, badge, photo 
8. Envoyer à Guillaume pour vérification, l'ajout de l'UBR, et l'ajout de sa signature
9. Envoyer à (Julie Beauchemin)[mailto:julie.beauchemin@uqtr.ca] (avec étudiant-e et Guillaume en copie)

!!!WARNING
Attention ! Les demandes de remboursements doivent être effectuées dans les 30 jours suivant l'événement.
!!!


# Bourse d'aide à la diffusion
L'UQTR offre des bourses d'aide à la diffusion pour les personnes au 1er et 3 cycle qui présentent des résultats scientifiques en congrès.

Voici les liens :
- **1er cycle** : https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=6756&owa_no_fiche=104&owa_bottin=
- **2e et 3e cycle** : https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=6756&owa_no_fiche=105&owa_bottin=

!!!success
N'oubliez pas, il s'agit d'une bourse qui peut être comptabilisée dans votre CV dans la partie Réconnaissance | Prix et distinction !
!!!