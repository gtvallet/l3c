---
visibility: hidden
label: Paie
order: 1000
author: GT Vallet
---


# Remboursement|Paie pour personnes externes

1. La personne doit compléter le rapport de dépenses et le signer
2. Joindre les pièces justificatives
3. Créer un compte sur le système de paie de l'UQTR

!!!TIPS
Indiquer sur le rapport de dépenses si préférence pour remboursement dans une autre devises (US, EUROS...)
!!!


Les personnes non-membres de l'UQTR doivent tout de même créer un compte sur le site de l'UQTR : http://www.uqtr.ca/depotdocumentpaie  

Il faut alors sélectionner "Utilisateurs externes" puis "Créer un compte"

![Capture 1. Inscription membres externes UQTR](/assets/UQTR_Rbsmt_Externes1.png)

![Capture 2. Création d'un nouveau compte](/assets/UQTR_Rbsmt_Externes2.png)

![Capture 3. Information à compléter](/assets/UQTR_Rbsmt_Externes3.png)


