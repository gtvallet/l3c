---
label: Kubios
order: 100
author: GT Vallet
---

# Kubios
!!!info
Kubios est un logiciel d'analyse de la fréquence cardiaque (FC) et de la variabilité de la fréquence cardiaque (VFC). 
Il permet de vérifier la qualité d'un enregistrement de FC ou de VFC, d'appliquer des corrections et surtout d'extraire les principaux indices pertients comme le RMSSD, les HF, etc.
Ce logiciel peut aussi s'utiliser sur smartphone pour effectuer l'enregistrement de la VFC et ajouter des marqueurs temporels (tags).
!!!

# Analyses
## Vérifications 
1. Vérifier visuellement la qualité du signal : régularité, détection des pics R [croix rouges]... (premier tracé, le plus en haut)
2. Noter le pourcentage de segments bruités ("Noise (h:min:s):") (information en haut à gauche) - corriger ces segments ("Remove all")
3. Repérer les zones du tracées comprenant des corrections, vérifier si l'ECG est en effet problématique ou non
4. Enlever la correction automatique si vous pouvez appliquer une correction manuelle. Autrement, appliquer le plus bas niveau de correction (`very low`) et noter le pourcentage de battements cardiaques auto-corrigé ("Corrected:") 

## Extraction 
1. Définir la fenêtre d'analyse ("Length (h:min:s)" à 4 min, descendre jusqu'à 3h30 si tracé très problématique) (deuxième tracé en haut). Déplacer la fenêtre d'analyse à la souris pour ne garder que les dernières minutes, vérifier si le signal semble bon (régualarité, etc.). Ajuster la fenêtre si nécessaire et documenter l'ajustement.
2. Ajouter autant d'échantillons (Samples) que nécessaire en répétant l'opération 1
3. Enregistrer l'analyse
4. Ouvrir le fichier ".csv", copier/coller les informations suivantes :
  - Time-Domain Results           
  	+ Mean HR (beats/min):        
  	+ SD HR (beats/min):          
  	+ RMSSD (ms):
  - Frequency-Domain Results - AR spectrum
	+ HF (ms$^2$):                  
  	+ HF (n.u.):                  
	(+ LF/HF ratio éventuellement)

!!!info
Il est possible d'extraire automatiquement les variables d'intérêts avec un script R, Python, Matlab, etc.
!!!

!!!success
Selon Laborde et al. (2017), il est recommandé d'utiliser les HF, absolute power, AR spectrum
!!!