---
label: R/RStudio
order: 120
author: GT Vallet
---

# R/RStudio
!!!info
R est langage de programme dédié aux analyses statistiques. Il est libre, gratuit et multiplateforme. 
!!!

## Installation
- R : https://cran.rstudio.com/
- RStudio : https://posit.co/download/rstudio-desktop/

## Packages recommandés
- tidyverse : https://www.tidyverse.org/
- easystats : https://github.com/easystats/easystats
- rmarkdown : https://rmarkdown.rstudio.com/
- summarytool
- performance
- afex
- psych
- questionR : https://juba.github.io/questionr/index.html

!!!info
Voir l'article de DellaData : https://delladata.fr/10-packages-pour-simplifier-vos-analyses-statistiques/
!!!

### Graphique avec GUI
- Esquisse : https://delladata.fr/decouvrez-laddin-esquisse/

### Ré-ordonner 
- QuestionR : https://juba.github.io/questionr/articles/recoding_addins.html

### Vérifier les conditions d'application
- Package performance
- Voir notamment pour des régressions : https://delladata.fr/tutoriel-regression-lineaire-multiple-r/

# Tuto
## Tuto interactifs
- Andy Field : https://www.statisticsadventure.com/

