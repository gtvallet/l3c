---
label: Sections
order: 1000
author: GT Vallet
---

# Rédaction des différentes sections d'un texte scientifique

Métaphore du sablier :
1. Introduction comme un entonnoir, du plus général au plus spécifique
2. Méthode et résultats comme colonne vertébrale, jonction entre les deux réceptacles du sablier
3. Discussion comme une pyramide, du plus spécidique au plus large


## Introduction

## Méthode
La section méthode présente l'ensemble des éléments qui permettent de répondre à la question de recherche.
Il s'agit autant des personnes recrutées et de leurs caractéristiques que du matériel utilisé et du déroulement de l'étude. 
Cette section contient aussi les informations concernant...  

## Résutlats

## Discussion 

1. Rappel de la problématique avec une ou deux phrases du rationnel associé
2. Rappel des lignes de la méthode (1 ou 2 phrases)
3. Rappel des hypothèses
4. Retour sur les hypothèses (validation, retour sur éléments de l'intro, ajout de nouvelles références)
5. Forces et limites
6. Perspectives