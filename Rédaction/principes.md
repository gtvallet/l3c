---
label: Principes
order: 1000
author: GT Vallet
---

# Principes généraux de la rédaction scientifique

## Avant-propos
- Rédaction scientiﬁque $\neq$ rédaction littéraire $\neq$ autre rédaction
- Conventions spéciﬁques (parfois implicites)
- Différences selon la culture disciplinaire considérée

!!!warning
Toujours bien lire et respecter les consignes aux personnes autrices !
!!!

## Principes généraux
- Clarté
- Précision
- Justesse
- Rigueur

## Structure canonique
1. Introduction
2. Méthode
3. Résultats
4. Discussion

## Conseils généraux
- Éviter les citations directes (niv. de compréhension)  
- Utiliser des tableaux et des **graphiques** (résultats, mais aussi procédure...)  
- Éviter les tournures passives (privilégier la voix active) sauf pour la méthode  
- Harmoniser les temps de conjugaison (présent pour faits généraux vs. passé autrement)  
- *Keep it simple*, éviter phrase propositionnelle, jargon, détails superflus...

## Conseils spécifiques
!!!important
Un paragraphe == 1 idée !
!!!

- Préparer un plan (idées principales + liens logiques entre ces idées)
- Déterminer son (ses) objectif(s) de communication
- Cibler le lectorat
- Considérer la portée du travail

!!!warning
Garder en tête les attendus. Un essai ou une thèse doit faire montre du niveau d'expertise de la personne, de l'étendue de ses connaissances, etc.
!!!

## CRediT

https://www.elsevier.com/researcher/author/policies-and-guidelines/credit-author-statement