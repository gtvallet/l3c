---
label: Le laboratoire (L3C)
order: 1000
author: GT Vallet
---

# Laboratoire Cognition-Corps-Contexte

Le laboratoire Cognition-Corps-Contexte (L3C) est dirigé par Guillaume T. Vallet, Ph.D. en tant que professeur à l'Université du Québec à Trois-Rivières (UQTR).
Le laboratoire a pour objectif d'étudier de la psychologie humaine selon une **approche incarnée et située de la cognition**.
Cette approche définit le fonctionnement cognitif comme étant intrinsèquement déterminé par les interactions entre un individu et son environnement (*énaction*).

Ainsi, les caractéristiques corporelles (capacités sensorielles, motrices, taille, physionomie...), l'état physiologique actuel (fatigue, énergie...), l'environnement immédiat (luminosité, bruit...) et le contexte social (culture, stéréotypes...) façonnent les pensées et les capacités de traitement de l'information d'un individu.

## Philosophie et valeurs
Le laboratoire prone des **valeurs de respect, d'ouverture, et de rigueur**.
Les recherches qui sont conduites s'inscrivent dans une démarche éthique et d'intégrité. 
Le laboratoire a à coeur de diffuser ses travaux à la fois au niveau de la communauté scientique et du grand-public. 
Le laboratoire encourage la diffusion de la recherche de manière ouverte et accessible, contribuant ainsi à l'enrichissement de la connaissance collective. 
Une partie des études vise d'ailleurs une application directe et pratique dans le souci d'avoir des retombées immédiates et sociales.

La collaboration est une valeur tout aussi essentielle au sein du laboratoire, que ce soit entre les collègues de l'équipe qu'avec des membres de l'univeristé et d'autres institutions du monde entier. 
Convaincu qu'à plusieurs, on va plus loin, et que différents points de vues construisent de meilleures recherches, chacun et chacune est invité-e à s'exprimer dans le respect des autres et de leurs opinions. 

Enfin, nous cultivons un environnement de travail inclusif, où la diversité des idées est encouragée et celle des personnes respectée, stimulant ainsi l'innovation et la créativité au sein de notre équipe.


## Historique et membres du laboratoire
Le laboratoire existe de manière informelle depuis août 2022 et il a été formalisé à l'automne 2024.
Le laboratoire est composé de membres professeurs, de personnes étudiantes et de personnes qui travaille en tant que professionnel et assistant de recherche. 
La liste des membres, actuels et passés, est visible sur la page officielle du laboratoire (à venir).
