---
label: Communication
order: 99
author: GT Vallet
---

# Communication interne

## Courriel
Le courriel est le principal outil de communication dans le monde académique.
Il vous est demandé de suivre les règles suivantes :
- utilisation des adresses professionnelles uniquement (type `@uqtr.ca`)
- utiliser un objet de message signifiant (par ex. `Demande de rendez-vous pour la semaine prochaine` et non juste `Rendez-vous`)
- débuter et terminer votre message par une formule de politesse
- utiliser un ton et des tournures professionnelles et non familières
- limiter le recours aux abbréviations à celles qui sont couramment admises dans le langage courant ou dans le laboratoire

!!!danger **Co-encadrement & collaboration**  
Merci d'ajouter en copie conforme (CC) tous les membres du projet impliqués dans l'échange en cours. Pensez à la fonction `répondre à tous` lorsque vous répondez à un message commun.
!!! 


## Teams
Le laboratoire utilse une application de messagerie synchrone/asynchrone facilitant le suivi des échanges par personne, par projet et/ou par thème : Microsoft Teams. 

!!!info
Vous serez invité à rejoindre les projets vous concernant ou à rejoindre un espace d'échange 1 à 1 si nécessaire. 
!!!

Teams est un outil moins formel que le courriel et il n'est donc pas attendu la même rigueur que pour ces derniers.

!!!danger **Mention**  
Merci d'interpeller les personnes concernées par vos messages sur Teams par une mention `@` afin de s'assurer que ces personnes recoivent une notification à propos du message. 
Astuce, utiliser `@teams` pour interpeller l'ensemble des membres d'une équipe.
!!! 


## Délai d'envoi et de réponse
### Délai d'envoi 
Un message nécessitant le traitement ou la préparation d'une information ne devrait pas être envoyé à la dernière minute, du moins lorsque cela est possible. 
Un délai d'*au moins 24h/48h* avant est attendu pour laisser le temps au destinataire de traiter le message et son contenu. 
Ainsi, merci d'anticiper les réunions de suivi de projet en préparant à l'avance les versions de travail des essais, articles, résumés, etc.

### Délai de réponse
Il est attendu que l'ensemble des membres du laboratoire soient réactifs vis-à-vis des courriels ou des messages qui leurs sont destinés. 
Ainsi, un délai de réponse de 1 à 4 jours ouvrables[^1] est attendu, sauf situation particulière.

Les messages qui sont envoyées en-dehors des heures de travail habituel ne doivent pas être considéré comme étant lu ou traité par le destinataire avant sa prochaine période de travail. 

!!!danger
Aucune réponse ne doit être attendu en fin de semaine, jour férié ou période de vacances. De même, il n'est pas attendu que les messages soient consultés sur un média personnel (ordinateur, cellulaire...) même si chacun et chacune est libre de le faire.
!!!


## Échange de fichiers

### Nomenclature
Merci de respecter la nomenclature suivante : `AAAAMMJJ_Projet_NomDuDocument` avec :
- AAAAMMJJ : année/mois/jours par exemple `20220615` pour le 15 juin 2022.
- Projet : nom de code du projet associé à ce document (si pertinent) ou votre initiale de prénom et votre nom de famille (ex. GVallet)
- NomDuDocument: nom du document

Lorsque le fichier est révisé par quelqu'un du laboratoire, il faut alors ajouter les initiales de cette personne au nom du fichier `AAAAMMJJ_Projet_NomDuDocument_gtv` pour une révision par Guillaume.
Il peut s'agir aussi d'ajouter un statut, par exemple, `signed` pour signé.

!!!info
Merci de privilégier l'anglais pour les noms de fichiers afin d'assurer leur universalité d'usage et éviter les accents qui peuvent causer des difficultés ou des erreurs pour les systèmes informatiques.
!!!

### Média d'échange de fichiers
Les fichiers peuvent être échangés ou partagés par plusieurs moyens qui sont à déterminer selon l'usage.
Ainsi, une conversation impliquant plusieurs personnes dont au moins l'une est extérieure au laboratoire devrait se faire via courriel.
Par contre, un échange dans le cadre d'un projet avec uniquement un/une ou des membres du laboratoire pourrait plutôt se faire via `Teams`.
Enfin, certains fichiers ou dossiers peuvent être davantage utilisés comme des ressources plus permanentes ou être tout simplement trop volumineux. 
Dans ces cas, l'idéal est d'utilisé une dossier/fichier partagé via `OneDrive` ou l'onglet `Fichiers` de `Teams`.

!!!info
 L'échange de fichiers volumineux peut aussi se faire grâce à des plateformes comme [Swiss Transfert](https://www.swisstransfer.com/fr).
!!!

---
[^1]: Les jours ouvrables désignent les jours habituellement travaillés, hors fin de semaine, jour férié, vacances...
