---
title: Organisation et données
---

## Structure d'un projet de recherche

Le laboratoire demande que vous suiviez cette organisation hiérachique pour votre (vos) projet de recherche. 
Cette structure sera celle employée dans les équipes Teams des projets de recherche. 

```
1-ProjectManagement
  |- 11_Proposals
  |- 12_Finance
  |- 13_Reports
2-Ethic
  |- 21_WorkingFiles
  |- 22_EthicsApproval
  |- 23_ConsentForms
3-Exp
  |- 31_ExpFiles
    |- 311_Material
    |- 312_Script
    |- 313_Npsy
  |- 32_Data
    |- 321_RawData
    |- 322_EditedData
    |- 323_FinalData
  |- 33_Stats
    |- 331_Tables
    |- 332_Figs
  |-34_Outputs
4-Publi
  |- 41_Confs
  |- 42_Articles
  |- 43_Other
```


## Nommenclature des fichiers

1. Date (AAAMMJJ)
2. Acronyme du projet
3. NomDuDocument
4. (Version : v1, v2)
5. (Éditeur : initiales comme `gtv`)
6. (Status : `signed`...)

!!!warning
Éviter les espaces (utiliser des _ ou des -) et les accents
Priviligier l'anglais pour l'interopérabilité et l'universalité d'accès
!!!


## Gestion des données

### Sauvegardes 

!!!important
Il est nécessaire de sauvegarder vos données de recherche, c'est un devoir éthique et légal !
!!!

Toutes vos données doivent être sauvegardées de manières régulières. 
Ainsi, vous devez téléverser des données expérimentales (fichiers `csv` de PsychoPy par ex.) la soirée même de la journée d'expérimentation sur Teams ou à défaut sur votre OneDrive.

À chaque fin de semaine, assurez-vous que toutes vos données (expérimentales, saisies des résultats des questionnaires, etc.) soient sur les fichiers de l'équipe Teams.


### Documents papiers

Merci de porter immédiatement après l'expérimentation (ou en fin de journée) ces documents dans un classeur fermant à clé dans les locaux du CogNAC.  
Il est important de séparer les documents identificatoires (par ex. formulaire d'information et de consentement) de ceux anonymes (code participant).

!!!warning
Vous ne devriez jamais sortir de l'université avec ce type de documents, ce serait une violation éthique.
!!!