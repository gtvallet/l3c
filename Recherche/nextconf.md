---
order: 98
authors: GT Vallet
visibility: protected
---

# Prochains congrès

## Au Québec
- [RISUQ](https://risuqcolloque2024.wixsite.com/colloquerisuq?fbclid=IwZXh0bgNhZW0CMTAAAR2dBTgRv0MVSR_UV8vwzm8f63CLfNQTTrlSjhS7fZ18M4y-UCfpPA-H4IQ_aem_ZmFrZWR1bW15MTZieXRlcw) : 14-15 novembre 2024
- [SQRP](https://www.sqrp.ca/) : 30 mai - 01 juin 2025
