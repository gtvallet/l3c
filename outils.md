---
label: Outils
order: 98
author: GT Vallet
---
# Outils recommandés
!!!
Cette page recense quelques outils qui sont recommandés par le laboratoire. Ce choix n'est pas guidé par des commandites, mais bien par une sélection selon l'expérience des membres du laboratoire. Merci de garder à l'esprit le fait que les membres du laboratoire ne sont pas obligés d'utiliser ces outils !
!!!


## Bureautique
- [Suite MSOffice](https://www.office.com/) (incluse à l'UQTR)
- [Suite LibreOffice](https://fr.libreoffice.org/) (gratuite et libre)
- [Markdown](https://www.markdownguide.org/) et [RMarkdown](https://rmarkdown.rstudio.com/) : usage avancé pour mise en forme automatique

!!!info
Mardown est un système très simple en apparence permettant de créer de multiple type de documents (article, thèse, présentation...) dans de multiples formats (PDF, Word, HTML...). C'est l'outil privilégié dans le laboratoire pour les rapports d'analyses.
!!!


## Bibliographie
- [Zotero](https://www.zotero.org/) (gratuit et libre). 
Voir aussi la page dédiée.

!!!info 
L'université propose plutôt l'usage de EndNote, mais ce logiciel n'est pas recommandé au sein du laboratoire.
!!!


## Analyses statistiques
- [JAMOVI](https://www.jamovi.org/) (gratuit et libre)
- [JASP](https://jasp-stats.org/) (gratuit et libre)  : analyses bayésienne
- [R/RStudio](https://posit.co/download/rstudio-desktop/) (gratuit et libre) : usage avancé, flexibilité et reproductibilité 

!!!info
La formation à l'université se concentre sur SPSS (achat à prix réduit possible à l'UQTR), mais ce logiciel n'est pas recommandé au sein du laboratoire.
!!!


## Expérimentation
- [OpenSesame](https://osdoc.cogsci.nl/) : interface graphique simple. Possibilité de l'utiliser pour conduire des études en ligne.
- [PsychoPy](https://www.psychopy.org/) : puissant logiciel de programmation d'expérimentation. Possibilité de l'utiliser pour conduire des études en ligne.
- [LimeSurvey](https://www.limesurvey.org/) : questionnaires en ligne. Voir le serveur du CogNAC. 


## OpenScience
- [OSF](https://osf.io/) (Open Science Framework)


## Partage de fichiers
- OneDrive (inclus à l'UQTR avec la suite Office365)
- [Swiss Transfert](https://www.swisstransfer.com/fr) pour l'envoi de fichiers volumineux


## Gestionnaire de tâches
- [Todoist](https://todoist.com/fr/home) : puissant logiciel de todo, cross-plateformes avec reconnaissance du langage naturel (comme "demain")
- [MS ToDo](https://to-do.office.com/tasks/) : intégration dans Teams et Outlook


## Divers
### Sécurité
- [BitWarden](https://bitwarden.com/) pour la gestion des mots de passe

### Orthographe, grammaire et syntaxe
- Antidote (inclus à l'UQTR) surtout pour le français
- Antidote et Grammarly pour l'anglais

### Communication
- Teams pour la messagerie asynchrone/synchrone
- Teams ou Zoom pour les visioconférences
- Thunderbird pour les courriels

## Médias
- Gimp --> retouche d'images
- Audacity --> création/retouche de sons