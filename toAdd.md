---
visibility: hidden
---

# Poster

    Définir la structure de l'affiche (format, sections, articulation visuelle)
    Définir le contenu de l'affiche (sélectionner que les informations pertinentes et essentielles)
    Se procurer des exemples de "bonnes affiches" (ex : https://www.animateyour.science/post/best-examples-of-scientific-posters OU https://mindthegraph.com/blog/award-winning-scientific-poster/)
    Consulter les tuto vidéos sur comment créer une affiche (site UQTR pour le concours d'affiches, etc.) - Par ex : https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=4192&owa_no_fiche=83&owa_bottin=



Info à ajouter par la suite

# Info sur rabais pour publication en accès libre
https://www.crkn-rcdr.ca/fr/publication-en-libre-acc%C3%A8s

# Liens bourses
file:///home/gvallet/Downloads/Calendrier_synthese_bourses_org._subventionnaires_2023-2024.pdf

# Guide
Je suis tombé là-dessus, pour ceux que ca concerne 🙂 
https://www.affairesuniversitaires.ca/conseils-carriere/prof-ou-pas/petit-guide-de-la-demande-de-bourse-parfaite-partie-1/?fbclid=IwAR2HWOKkrAk9Triy5TuwsTVJf2gfRaeY50kT6U8FkSVsucl8CDM0SvAAaSo



# Recherche journal
https://www.scimagojr.com/

# Metric et recherche biblio
Métrique avec Dimension IA
https://www.youtube.com/watch?v=2IKbRm5cgTw

# Organisation des dossiers de recherche

```
1-ProjectManagement
  |- 11_Proposals
  |- 12_Finance
  |- 13_Reports
2-Ethic
  |- 21_WorkingFiles
  |- 22_EthicsApproval
  |- 23_ConsentForms
3-Exp
  |- 31_ExpFiles
    |- 311_Material
    |- 312_Script
    |- 313_Npsy
  |- 32_Data
    |- 321_RawData
    |- 322_EditedData
    |- 323_FinalData
  |- 33_Stats
    |- 331_Tables
    |- 332_Figs
  |-34_Outputs
4-Publi
  |- 41_Confs
  |- 42_Articles
  |- 43_Other
```

# Bonnes pratiques tableurs
https://guides.library.stanford.edu/data-best-practices/manage-spreadsheets

# Données sensibles
https://guides.library.stanford.edu/data-best-practices/sensitive-data

# nettoyage base de données
https://openrefine.org/

# Questionnaires en lignes vs non
https://espace.inrs.ca/id/eprint/2678/1/Inedit02-15.pdf

# Calcul puissance
https://cran.r-project.org/web/packages/pwrss/vignettes/examples.html

# Financement au doct
https://www.milliondollarphd.ca/preview#preview

# Réf intéressante pour conseils vie académique
http://www.culhamlab.com/academic-advice

# Info remboursement affiches etc

# Procedure demande avance de fonds
1. Ouvrir le fichier “Demande d’avance”
2. Indiquer l’étudiant-e comme réquérant
3. Indiquer le montant demander et justifier la demande
4. Indiquer le numéro de certificat éthique 
5. Indiquer l’UBR/Annexe C
6. Faire compléter numéro de matricule (si déjà eu un contrat avec univ, sinon indiquer adresse postale complète)et signature par étudiant-e
7. Envoyer à Julie Beauchemin avec autorisation use fonds

# Formation plagiat
https://oraprdnt.uqtr.uquebec.ca/portail/gscw031?owa_no_site=462


# Biblio
https://www.researchrabbit.ai/